﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XNAUI.Base;
using XNAUI.Elements;

namespace XNAUI.Inventory
{
    
    public class InventoryGUI : Drawable, IObserver<Inventory>, MouseSensitive, Updateable
    {
        List<InventoryButton> buttons;
        public static Button slideTab;

        public static int buttonWidth = 70;
        public static int buttonHeight = 70;
        public static int hspacing = 10;
        public static int verticalOffset;
        public static int horizontalOffset;
        public static SpriteFont spriteFont;

        private int _inventoryWidth;
        public int inventoryWidth {
            get{
                return _inventoryWidth;
            }
            set{
                this._inventoryWidth = value;
                slideTab.rectangle = new Rectangle(0, 0, _inventoryWidth, _inventoryHeight);
            }
        }
        private int _inventoryHeight;
        public int inventoryHeight
        {
            get
            {
                return _inventoryHeight;
            }
            set
            {
                _inventoryHeight = value;
                slideTab.rectangle = new Rectangle(0, 0, _inventoryWidth, _inventoryHeight);
            }
        }


        public InventoryGUI()
        {
            buttons = new List<InventoryButton>();
            slideTab = new Button(new Rectangle(0, 0, inventoryWidth, inventoryHeight));
            horizontalOffset = 5;
            verticalOffset = 5;
            slideTab.maxAlpha = 0;
            slideTab.hovered = inventoryHoverCallback;
            slideTab.unhovered = inventoryUnhoverCallback;
        }

        void inventoryHoverCallback()
        {
            if (slideTab.maxAlpha < 1.0f)
            {
                slideTab.maxAlpha += 0.04f;
            }
            foreach(InventoryButton button in buttons)
            {
                if (button.maxAlpha < 1.0f)
                {
                    button.maxAlpha += 0.04f;
                }
            }
        }

        void inventoryUnhoverCallback()
        {
            if (!Inventory.Instance.isItemSelected())
            {

                if (slideTab.maxAlpha > 0)
                {
                    slideTab.maxAlpha -= 0.04f;
                }
                else
                {
                    slideTab.maxAlpha = 0;
                }

                foreach (InventoryButton button in buttons)
                {
                    if (button.maxAlpha > 0)
                    {
                        button.maxAlpha -= 0.04f;
                    }
                    else
                    {
                        button.maxAlpha = 0;
                    }
                }
            }
        }


        public void setInventoryTexture(Texture2D texture)
        {
            slideTab.Icon = texture;
        }

        public void setInventoryFont(SpriteFont spriteFont)
        {
            InventoryGUI.spriteFont = spriteFont;
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(Inventory value)
        {
            updateButtons();
        }

        private void updateButtons()
        {
            List<Item> inventoryItems = Inventory.Instance.getItems();
            bool found;
            if (inventoryItems.Count > this.buttons.Count)
            {
                for (int i = 0; i < inventoryItems.Count; i++)
                {
                    found = false;
                    for (int j = 0; j < this.buttons.Count; j++)
                    {
                        if (inventoryItems[i].Equals(this.buttons[j].item))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        Rectangle rectangle = new Rectangle(horizontalOffset + (hspacing + buttonWidth) * i, verticalOffset, buttonWidth, buttonHeight);
                        InventoryButton button = new InventoryButton(rectangle, inventoryItems[i]);
                        buttons.Add(button);
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < this.buttons.Count; i++)
                {
                    found = false;
                    for (int j = 0; j < inventoryItems.Count; j++)
                    {
                        if (inventoryItems[i].Equals(this.buttons[i].item))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        this.buttons.RemoveAt(i);
                        for (int j = i; j < this.buttons.Count; j++)
                        {
                            this.buttons[j].targetXpos -= buttonWidth;
                        }
                    }
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (InventoryButton button in buttons)
            {
                button.Update(gameTime);
            }
            processMouse(Mouse.GetState());
        }

        public void processMouse(MouseState state)
        {
            slideTab.processMouse(state);
            foreach (InventoryButton button in buttons)
            {
                button.processMouse(state);
            }
        }

        public void Draw(SpriteBatch batch)
        {
            slideTab.Draw(batch);
            foreach (InventoryButton button in buttons)
            {
                button.Draw(batch);
            }
        }
    }
}
