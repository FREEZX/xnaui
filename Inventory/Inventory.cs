﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XNAUI.Inventory
{
    public class Inventory : Observable<Inventory>
    {
        List<Item> inventoryItems;
        int _selectedItem = -1;
        static Inventory _instance;

        public bool isItemSelected()
        {
            if (_selectedItem == -1)
            {
                return false;
            }
            return true;
        }

        Inventory()
        {
            inventoryItems = new List<Item>();
        }

        public static Inventory Instance{
            get{
                if (_instance == null)
                {
                    _instance = new Inventory();
                }
                return _instance;
            }
        }

        public List<Item> getItems()
        {
            return inventoryItems;
        }

        public Item getSelectedItem()
        {
            if (_selectedItem != -1)
            {
                return inventoryItems[_selectedItem];
            }
            return null;
        }

        public void selectItem(String itemName)
        {
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                if (inventoryItems.ElementAt(i).name == itemName)
                {
                    _selectedItem = i;
                    break;
                }
            }
        }

        public void selectItem(Item item)
        {
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                if (inventoryItems.ElementAt(i).Equals(item))
                {
                    _selectedItem = i;
                    break;
                }
            }
        }

        public void clearSelection()
        {
            _selectedItem = -1;
        }

        public void addItem(Item item)
        {
            inventoryItems.Add(item);
            Notify(this);
        }

        public void removeItem(String itemName)
        {
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                if (inventoryItems.ElementAt(i).name == itemName)
                {
                    inventoryItems.RemoveAt(i);
                    break;
                }
            }
            Notify(this);
        }

        public void removeItem(Item item)
        {
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                if (inventoryItems.ElementAt(i).Equals(item))
                {
                    inventoryItems.RemoveAt(i);
                    break;
                }
            }
            Notify(this);
        }
    }
}
