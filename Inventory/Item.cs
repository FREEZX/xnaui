﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XNAUI.Inventory
{
    public class Item : IEquatable<Item>
    {
        public Texture2D itemIcon;
        public String name;
        public String description;

        public Item(String name, String description, Texture2D texture)
        {
            this.name = name;
            this.description = description;
            this.itemIcon = texture;
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public bool Equals(Item obj)
        {
            if (obj!=null && obj.name == this.name)
            {
                return true;
            }
            return false;
        }
    }
}
