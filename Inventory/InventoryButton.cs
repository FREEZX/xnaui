﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XNAUI.Elements;
using XNAUI.Globals;

namespace XNAUI.Inventory
{
    public class InventoryButton : Button, IEquatable<InventoryButton>
    {
        public Item _item;
        public float maxTextAlpha = 0.0f;
        public Item item{
            set{
                this.Icon = value.itemIcon;
                _item = value;
            }
            get{
                return _item;
            }
        }

        public InventoryButton(Rectangle rectangle, Item item)
            : base(rectangle)
        {
            this._item = item;
            this.Icon = item.itemIcon;
            this.onClick = ItemButtonClicked;
        }

        void ItemButtonClicked()
        {
            Inventory.Instance.selectItem(_item);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (this.hoverState == Button.STATE_HOVERED || this.item.Equals(Inventory.Instance.getSelectedItem()))
            {
                if (maxTextAlpha < 1)
                {
                    maxTextAlpha += 0.05f;
                }
            }
            else
            {
                if (maxTextAlpha > 0)
                {
                    maxTextAlpha -= 0.05f;
                }
                else
                {
                    maxTextAlpha = 0;
                }
            }
        }

        public override int GetHashCode()
        {
            return _item.GetHashCode();
        }

        public bool Equals(InventoryButton obj)
        {
            if (obj._item.Equals(this._item))
            {
                return true;
            }
            return false;
            
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);
            if (InventoryGUI.spriteFont != null)
            {
                Vector2 rectDim = InventoryGUI.spriteFont.MeasureString(item.name);
                Rectangle rect = new Rectangle(this.rectangle.X - 5, this.rectangle.Y + this.rectangle.Height + InventoryGUI.verticalOffset, (int)rectDim.X + 10, (int)rectDim.Y);
                batch.Draw(InventoryGUI.slideTab.Icon, rect, Color.Black * maxTextAlpha);
                batch.DrawString(InventoryGUI.spriteFont, item.name, new Vector2(this.rectangle.X, this.rectangle.Y + this.rectangle.Height + InventoryGUI.verticalOffset), Color.White * maxTextAlpha);
            }
        }
        
    }
}
