﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XNAUI.Elements
{
    public interface MouseSensitive
    {

        void processMouse(MouseState state);
    }
}
