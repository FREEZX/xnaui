﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XNAUI.Base;

namespace XNAUI.Elements
{
   public class Button : MouseSensitive, Drawable, Updateable
    {
        private bool _clickState = false;
        private bool _held = false;
        private int _hoverState = 0;
        public int hoverState
        {
            get
            {
                return _hoverState;
            }
        }

        public int targetXpos;
        public int targetYpos;

        private DateTime enterTime;
        public Rectangle rectangle {set; get;}
        public Texture2D Icon {set; get;}
        public float maxAlpha = 1;
        public Color drawColor = Color.White;

       //
        public readonly static int STATE_HOVERED = 1;
        public readonly static int STATE_UNHOVERED = 0;

        

        //CALLBACKS
        public OnClick onClick;
        public OnRelease onRelease;
        public OnHover onHover;
        public OnHold onHold;
        public Hovered hovered;
        public Unhovered unhovered;
        
        public Button(){
            this.rectangle = new Rectangle();
        }


        public Button(Rectangle rectangle)
        {
            // TODO: Complete member initialization
            this.rectangle = rectangle;
            this.targetXpos = rectangle.X;
            this.targetYpos = rectangle.Y;
        }

        public virtual void Update(GameTime gameTime)
        {
            if (targetXpos != rectangle.X)
            {
                rectangle = new Rectangle((int)MathHelper.Lerp(targetXpos, rectangle.X, 0.95f), (int)MathHelper.Lerp(targetYpos, rectangle.Y, 0.2f), rectangle.Width, rectangle.Height);
            }
            processMouse(Mouse.GetState());
        }

        public void processMouse(MouseState state)
        {
            Point mousePosition = new Point(state.X, state.Y);
            if (rectangle.Contains(mousePosition))
            {
                if (hovered != null)
                {
                    hovered();
                }
                if (state.LeftButton == ButtonState.Pressed)
                {
                    if (_clickState)
                    {
                        if ((DateTime.Now.Subtract(enterTime).CompareTo(new TimeSpan(0, 0, 2)) == -1) && _held == false)
                        {
                            _held = true;
                            if (this.onHold != null)
                            {
                                onHold();
                            }
                        }
                    }
                    else
                    {
                        if (this.onClick != null)
                        {
                            onClick();
                        }
                        enterTime = DateTime.Now;
                    }
                    _clickState = true;
                }
                else
                {
                    if (_clickState)
                    {
                        if (this.onRelease != null)
                        {
                            this.onRelease();
                        }
                    }
                    else if (_hoverState == STATE_UNHOVERED)
                    {
                        if (this.onHover != null)
                        {
                            this.onHover();
                        }
                    }
                    _clickState = false;
                }
                _hoverState = STATE_HOVERED;
            }
            else{
                if (unhovered != null)
                {
                    unhovered();
                }
                _hoverState = STATE_UNHOVERED;
                _clickState = false;
            }
        }
        public virtual void Draw(SpriteBatch batch){
            if(Icon!=null){
                batch.Draw(Icon, rectangle, drawColor * maxAlpha);
            }
        }
    }
}
