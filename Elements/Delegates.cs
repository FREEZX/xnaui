﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XNAUI.Elements
{
    public delegate void OnClick();
    public delegate void OnHold();
    public delegate void OnRelease();
    public delegate void OnHover();
    public delegate void Hovered();
    public delegate void Unhovered();
}