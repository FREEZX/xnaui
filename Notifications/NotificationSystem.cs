﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XNAUI.Elements;

namespace XNAUI.Notifications
{
    class Notifications : Drawable
    {
        String currentText;
        float maxAlpha;

        private Notifications()
        {
        }
        Notifications _instance;
        Notifications Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Notifications();
                }
                return _instance;
            }
        }
        void setNotification(String notification)
        {
            this.currentText = notification;
            this.maxAlpha = 1;
        }
    }
}
