﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XNAUI.State
{
    public class StateManager
    {
        List<Level> stateList;
        int currentState = 0;

        static StateManager instance = null;

        private StateManager()
        {
            stateList = new List<Level>();
        }

        public static StateManager Instance
        {
            get{
                if(instance == null){
                    instance = new StateManager();
                }
                return instance;
            }
        }

        public List<Level> getStates()
        {
            return stateList;
        }

        public void AddState(Level level)
        {
            stateList.Add(level);
        }

        public void changeState(String name)
        {
            stateList[currentState].SwitchFrom();
            for (int i=0; i<stateList.Count; i++)
            {
                if (stateList[i].name.Equals(name))
                {
                    currentState = i;
                    break;
                }
            }
            stateList[currentState].SwitchTo();
        }

        public Level getState()
        {
            return stateList[currentState];
        }
        
    }
}
