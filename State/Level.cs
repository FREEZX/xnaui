﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XNAUI.State
{
    abstract public class Level
    {
        public String name;
        abstract public void Initialize(ContentManager Content);
        abstract public void Update();
        abstract public void Draw(ref SpriteBatch _spriteBatch);
        public virtual void SwitchTo() { }
        public virtual void SwitchFrom() { }
        public virtual void Load() { }
        public virtual void Unload() { }
    }
}
